The Locations application allows a user to create a list of locations and view
location details.

Instructions:

Implement the requirements below.

Part of the UI has been stubbed out but will need to be replaced in order
to make the application work.

An IoC container is not required, but interfaces and dependency injection should
be used where it makes sense.

Requirements:

- Locations may be saved to a database or in-memory
- Locations are NOT unique to a user. If two users are adding locations at the same
	time then both users should see the locations list being updated with all changes.
- Add Location Form
	- Name field is required
	- Address field is required
	- When form is submitted the location's coordinates must be found
		- If the coordinates are found the location is saved, then the user is
			redirected back to the form with a message informing them that the 
			location was saved.
		- If the coordinates are not found the form is displayed back to the user
			with an error message informing them that the location could
			not be located.
- Locations List
	- List all saved locations ordered by name
	- Each item should link to a location details page
- Location Details Page
	- Should display name and address
	- Display a map, below the name and address, with a marker at the location's coordinates